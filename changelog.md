#test_worker
## 2015/11/07 0.0.1 DLV initial commit 
1. initial commit
## 2015/11/07 0.0.2 DLV  publish
1. added cli
## 2015/11/08 0.0.3 DLV  publish
1. clean up
## 2015/11/08 0.0.4 DLV  publish
1. adding mocha tests
## 2015/11/08 0.0.5 DLV  publish
1. Display mocha tests with suite information now
## 2015/11/08 0.0.6 DLV  publish
1. Clean up display
## 2015/11/14 0.0.7 DLV  publish
1. Refactored to use latest serial-jasmine
## 2015/11/21 0.0.8 DLV  publish
1. viewer.html was using let instead of var broke safari
## 2015/11/22 0.0.9 DLV  publish
1. postinstall failing when trying to copy files to higher directory
1. viewer.html and config.json will now be in node_modules directory
## 2015/11/22 0.0.10 DLV  publish
1. postinstall failing when trying to copy files to higher directory
1. didn't catch one place

## 2015/11/27 0.0.11 DLV  publish
1. latest version of expect fails on global install
## 2015/11/27 0.0.1 DLV  publish
1. updated verion of serial-mocha



