/**
 * @module index.js
 *
 **/
'use strict';
var Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	child_process = Promise.promisifyAll(require("child_process")),
	util = require("util"),
	Queue = require("queue_aws"),
	path = require("path");
var noop=()=>void(0);

class Worker {
	constructor(options,config) {
		options = options || {};
		if (!options.awsCredentials) {  //will use envars but easier to add sub hash here than test later
			options.awsCredentials = {}
		}
		if(!config) {
			try {
				this.config = require("../../../config.json")
			} catch (ex) {
				this.config = require("./config_skeleton.json");
			}
		}else{
			this.config=config;
		}
		this.aQueues = new Map();
		this.sendOptions = {
			awsRegion         : options.awsRegion || process.env.AWS_REGION,
			awsAccessKeyId    : options.awsCredentials.awsAccessKeyId || process.env.AWS_ACCESS_KEY_ID,
			awsSecretAccessKey: options.awsCredentials.awsSecretAccessKey || process.env.AWS_SECRET_ACCESS_KEY
		};
		this.receiveOptions = {
			queueName         : this.config.queueName,
			urlQueueSend      : options.outboundQueueUrlBase + "/" + this.config.queueName,
			urlQueueReceive   : options.outboundQueueUrlBase + "/" + this.config.queueName,
			awsRegion         : options.awsRegion || process.env.AWS_REGION,
			awsAccessKeyId    : options.awsCredentials.awsAccessKeyId || process.env.AWS_ACCESS_KEY_ID,
			awsSecretAccessKey: options.awsCredentials.awsSecretAccessKey || process.env.AWS_SECRET_ACCESS_KEY
		};
		this.listenQueue = void(0);
	}

	getSendQueue(testerUrl) {
		let theQueue = void(0);
		if (!(theQueue = this.aQueues.get(testerUrl))) {
			let newOptions = {
				urlQueueSend   : testerUrl,
				urlQueueReceive: testerUrl
			};
			Object.keys(this.sendOptions).forEach((key)=>newOptions[key] = this.sendOptions[key]);
			this.aQueues.set(testerUrl, theQueue = new Queue(newOptions));
		}
		return theQueue
	}

	getTests(testerUrl) {
		let test = this.config;
		this.getSendQueue(testerUrl).sendMessage(JSON.stringify(test))
			.then((results)=>{
				//console.log(results);
			})
			.catch((err)=>console.log(err))
	}

	startListener() {
		let self = this;
		let noop=()=>void(0);
		this.listenQueue = new Queue(this.receiveOptions);
		this.listenQueue.initializeQueue()
			.then((q)=> {
				let ql = Promise.coroutine(function* () {
					while (true) {
						yield Promise.delay(self.config.pollInterval || 10000);
						yield self.listenQueue.readMessage()
							.then((results)=> {
								// read the messages
								results.forEach((result)=> {
									var message = result;
									var body = JSON.parse(message.Body);
									switch (body.request) {
										case "getTests":
										{
											self.getTests(body.testerUrl);
											self.listenQueue.removeMessage(message)
												.then(noop)
												.catch((err)=>console.log(err, err.stack));
											break;
										}
										case "runJasmineTests":
										{
											//console.log("running jasmine test");
											require("./lib/runJasmineTests")(body,self,message);
											break;
										}
										case "runMochaTests":
										{
											//console.log("running mocha test");
											require("./lib/runMochaTests")(body,self,message);
											break;
										}
										case "runTest":
										{
											require("./lib/runTest")(body,self,message);
											break;
										}
										case "stopListener":
										{
											self.listenQueue.removeMessage(message)
												.then((result)=> self.listenQueue = null)
												.catch((err)=>console.log(err, err.stack));
											break;
										}
										default:
										{
											console.log("not implemented");
											self.listenQueue.removeMessage(message)
												.then((result)=>console.log(result))
												.catch((err)=>console.log(err, err.stack));
										}
									}

								});
							})
							.catch((err)=>console.log(err, err.stack))
					}
				});
				ql();
			})
			.catch((err)=> {
				throw err
			})
	}
}
module.exports = Worker;
