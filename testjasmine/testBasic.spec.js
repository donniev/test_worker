/**
 * module testBasic.spec
 *
 */
'use strict';
describe("firstSuite",()=> {
	it("firstTest", (done)=> {
		expect(true).toBe(true);
		expect("one").toEqual("one");
		done();
		//console.log("one")
	})
	describe("Nested Suite",(done)=>{
		it("first test nested suite",(done)=>{
			expect(12).toBe(12);
			expect(null).toEqual(null);
			//console.log("two");
			setTimeout(()=>done(),1000)
		})
		describe("Nested Nested Suite",(done)=>{
			it("Nested nested suite test",(done)=>{
				expect(12).toBe(12);
				expect(null).toEqual(null);
				//console.log("two");
				setTimeout(()=>done(),1000)
			})



		})



	})
})
	describe("Second Suite",(done)=>{
	it("second suite test",(done)=>{
		expect(12).toBe(12);
		expect(null).toEqual(null);
		//console.log("two");
		setTimeout(()=>done(),1000)
	})



})