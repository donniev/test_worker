/**
 * module postinstall
 *
 */
'use strict';
var Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	path = require("path");
let targetDirectory = path.resolve(__dirname);
let sourceFile = path.resolve(__dirname + "/config_skeleton.json");
let copyfile = ()=>fs.createReadStream(sourceFile).pipe(fs.createWriteStream(targetDirectory + "/config.json"));
fs.readFileAsync(targetDirectory + "/config.json", "utf8")
	.then((results)=> { //we found it so skip copy
	})
	.catch((err)=> copyfile()); //error is good cannot find config file

let sk = process.env.AWS_SECRET_ACCESS_KEY,
	ak = process.env.AWS_ACCESS_KEY_ID,
	reg = process.env.AWS_REGION;
let content = fs.readFileSync("./index_skel.html", "utf8");
content = content.replace('${ACCESS_KEY}', ak ).replace('${SECRET_ACCESS_KEY}',  sk ).replace('${REGION}',  reg);
fs.writeFileSync("./viewer.html", content);