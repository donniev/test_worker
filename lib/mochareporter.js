/**
 * module mochareporter
 *
 */
'use strict';
class Reporter{
	constructor(runner,options){

		//console.log(8,runner);
		let ee =options.reporterOptions.emitter;
		runner.on("start",(data)=>{
			//console.log(12,"start",data);
			ee.emit("mochaStarted",{message:"mochaStarted",time:new Date()})
		});
		runner.on("suite",(suite)=>{
			ee.emit("suiteStarted",{message:suite})
		//console.log(17,"suite",suite);
		});
		runner.on("suite end",(data)=>{
			ee.emit("suiteDone",{message:"suiteDone",time:new Date()})
		//console.log(21,"suite end",data)
		});
		runner.on("pending",(test)=>{
			ee.emit("specDone",{message:test,status:"pending"})
		//console.log(25,"specDone","pending",test);
		});
		runner.on("pass",(test)=>{
			ee.emit("specDone",{message:test,status:"passed"})
		//console.log(29,"specDone","pass",test);
		});
		runner.on("fail",(test,err)=>{
			//console.log(32,"specDone","fail",test);
			ee.emit("specDone",{message:test,status:"failed",error:err})
		});
		runner.on("end",()=>{
			//console.log(36,"mochadone","done");
			ee.emit("mochaDone",{message:"mochaDone",time:new Date()})
		});
	}
}
module.exports=Reporter;
