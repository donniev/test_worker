/**
 * module runTest
 *
 */
'use strict';
var noop=()=>void(0);
module.exports=(body,self,message)=> {
	require(path.resolve(body.files))()
		.then((results)=> {
			let payload = {request: body.request, results: results};
			self.getSendQueue(body.testerUrl).sendMessage(JSON.stringify(payload))
				.then((results)=>console.log(results))
				.catch((err)=>console.log(err));

		})
		.catch((err)=> {
			self.getSendQueue(body.testerUrl).sendMessage(JSON.stringify(err))
				.then(noop)
				.catch((err)=>console.log(err));
		})
		.finally(()=> {
			self.listenQueue.removeMessage(message)
				.then(noop)
				.catch((err)=>console.log(err, err.stack));
		});


}