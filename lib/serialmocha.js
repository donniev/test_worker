/**
 * module serialmocha
 *
 */
'use strict';
var Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	child_process = Promise.promisifyAll(require("child_process")),
	util = require("util"),
	Mocha = require("mocha"),
	path = require("path"),
	EventEmitter = require("events").EventEmitter;
var target = void(0);

module.exports.runTasks = (files)=>new Promise((resolve, reject)=> {

	let promises = [];
	var out = new Map();
	if (typeof files === 'string') {
		files = [files];
	}
	let mocha = new Mocha();
	files.forEach((file)=> {


		file = path.resolve(file);
		//console.log("using file", file);
		promises.push(new Promise((res, reject)=> {
			let theTest = file;
			target = {
				mochaStarted: [],
				suiteStarted: new Map(),
				specStarted : new Map(),
				specDone    : new Map(),
				suiteDone   : [],
				mochaDone   : []
			};
			let finish = ()=> {
				console.log("finished",file,target.specDone.size);
				if(target.specDone.size) {
					out.set(file, target);
					ee = null;
					res(true)
				}
			}
			let ee = new EventEmitter();
			ee.on("mochaStarted", (data)=>target.mochaStarted.push(data));
			ee.on("suiteStarted", (data)=>{
				if(data.message.title) {
					target.suiteStarted.set(data.message.title, data.message.file)
					//console.log(46, target);
				}
			});
			ee.on("specDone", (data)=>{
				target.specDone.set(data.message.title, {testName:data.message.title,status:data.message.state,reason:data.error?data.error.message:""});
				//console.log(51,target);
			});
			ee.on("suiteDone", (data)=>target.suiteDone.push(data));
			ee.on("mochaDone", (data)=> {
				//console.log("got mochadone from reporter",data);
				target.suiteDone.push(data);
				finish()
			});
			//console.log("instantiating mocha");
			let mr = require("./mochareporter");
			//console.log(mr, typeof mr);
			mocha.reporter(mr,{emitter: ee});
			//console.log("instantiated reporter", mocha._reporter);
			mocha.addFile(file);
			//console.log(47, "running mocha");
			mocha.run(function (results) {
				//console.log(49, "mocha callback", results);
				finish();
			})
		}));
	});
	Promise.all(promises)
		.then((results)=> {
			//console.log("all settled,out",out);
			resolve(out)
		})

})