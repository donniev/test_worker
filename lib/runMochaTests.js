var noop = ()=>void(0);
module.exports = (body, self, message)=> {
	require("glob-promise")(body.files)
		.then((files)=> {
			require("serial-mocha").runTasks(files, null, true)
				.then((results)=> {
					let res = results;
					let payload = {request: body.request, results: res};
					self.getSendQueue(body.testerUrl).sendMessage(JSON.stringify(payload))
						.then(noop)
						.catch((err)=>console.log(err));
				})
				.catch((err)=> {
					self.getSendQueue(body.testerUrl).sendMessage(JSON.stringify(err))
						.then(noop)
						.catch((err)=>console.log(err));
				})
				.finally(()=> {
					self.listenQueue.removeMessage(message)
						.then(noop)
						.catch((err)=>console.log(err, err.stack));
				});
		})
		.catch((err)=> {
			self.getSendQueue(body.testerUrl).sendMessage(JSON.stringify(err))
				.then(noop)
				.catch((err)=>console.log(err));
			self.listenQueue.removeMessage(message)
				.then(noop)
				.catch((err)=>console.log(err, err.stack));
		});
}