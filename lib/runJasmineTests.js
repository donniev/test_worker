'use strict';
var noop=()=>void(0);
var writeResults = (results) => {
	//console.log(util.inspect(results,{depth:null}));
	let out = [];
	Object.keys(results).forEach(function (test) {
		let _tmp = {};
		_tmp.testFile = path.resolve(test);
		_tmp.tests = [];
		var item = results[test].results;
		Object.keys(item).forEach(function (region) {
			if (item[region] && item[region] instanceof Map) {
				item[region].forEach(function (val, key) {
					if (val.status && val.status === 'passed') {
						let nme = val.id.indexOf('spec') !== -1 ? "Test " : "Suite ";
						_tmp.tests.push({testName: nme + val.fullName, status: "passed", reason: ""});
					}
					if (val.status === 'failed') {
						let nme = val.id.indexOf('spec') !== -1 ? "Test " : "Suite ";
						if (val.failedExpectations) {
							let reasons = val.failedExpectations.map((expectation)=> expectation.message);
							_tmp.tests.push({testName: nme, status: "failed", reason: reasons.join("\n")})
						}
					}
				});

				//}
			}
		});
		out.push(_tmp);
	});
	return out;
};

module.exports=(body,self,message)=> {
	require("glob-promise")(body.files)
		.then((files)=> {
			require("serial-jasmine").runTasks(files, null, true,true,false)
				.then((results)=> {
					//let res = writeResults(results);
					let payload = {request: body.request, results: results};
					self.getSendQueue(body.testerUrl).sendMessage(JSON.stringify(payload))
						.then(noop)
						.catch((err)=>console.log(err));

				})
				.catch((err)=> {
					self.getSendQueue(body.testerUrl).sendMessage(JSON.stringify(err))
						.then(noop)
						.catch((err)=>console.log(err));
				})
				.finally(()=> {
					self.listenQueue.removeMessage(message)
						.then(noop)
						.catch((err)=>console.log(err, err.stack));
				});
		})
		.catch((err)=> {
			self.getSendQueue(body.testerUrl).sendMessage(JSON.stringify(err))
				.then(noop)
				.catch((err)=>console.log(err));
			self.listenQueue.removeMessage(message)
				.then(noop)
				.catch((err)=>console.log(err, err.stack));
		});
}/**
 * module runJasmineTests
 *
 */