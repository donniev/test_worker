#aws-test-worker

A worker module for running jasmine, mocha (and other) tests.
It runs a test when it receives a request on an AWS SQS queue.
It returns the results to the requestor via another SQS message.
You can use the module within node but it much easier to just start it from the command line.
To be useful you must have another application that sends requests for test results. This need
not be a node app. Anything that can send and receive an SQS message will work.
We provide a simple html file with the necessary javascript to request and view the tests.
That file can live anywhere and need not be on the same network as the worker app.

##Installation

```
npm install aws-test_worker   //for cli install globally
```
After installation there will be two new files in the root of your app

1. config.json - the file which defines the tests you will run when requested, the name of the queue to which
you will listen. If the queue doesn't exist it will be created, and the interval to pause between polling sqs

2. viewer.html - you can move this file anywhere you like and view test results. If you did not have aws credentials
you will have to add them to the file. Since the credentials are shown in clear text you should not serve it publically.
In fact it is not necessary to "serve" it at all. It is a self contained static file and you can just open it in a browser.
The worker file is very rudimentary. It queries AWS for a list of worker queues and displays them in a drop down.
When you select a worker, it is queried for tests available, displays them, and runs all the tests. It then displays
the results of the tests.


##Usage

Command line (requires global installation)

```
aws-test-worker --configfile <path to your config file should be in json format>  &
```
The & allows it to run in the background

Programatically

```
let Worker=require("aws-test_worker");
let aws = {}; // fill out with values if you don't have them set in environment variables
let config = require("./config.json"); //or some other config file or created in code
let worker = new Worker(aws,config);
worker.startListener();
```


##Methods:

Except for startListener and stopListener you don't call these methods directly.
When the listener receives an sqs message it will run the appropriate method and
send the results to the sender via another sqs message.

1. getTests - returns the test your worker can run (i.e. the testFiles in your config.json
1. startListener - starts listening for requests to run tests
1. runJasmineTests - runs the jasmine tests.
1. runTest - run a non jasmine test (see below for test requirements
1. stopListener - stop listening

After running tests it will capture results and send them to an aws sqs queue and delete the incoming
message from the queue so it is not reprocessed.

##Format of aws hash

```
{
	awsRegion:xxx,
	awsCredentials:{
		awsAccessKeyId:xxx,
		awsSecretAccessKey:xxx
	}
}
```

Alternatively you can provived an empty hash for aws IF you have those values in environment variables.

You should also copy config_skeleton.json to config.json which should live in the root of your app
and modify config.json to fit your environment. You can also just pass a hash of the appropriate shape into
the worker constructor rather than keeping it in a file.


```
{
	"testFiles":[
		{
			"files"    : "test/*.spec.js",
			"testName" : "jasmine",
			"isJasmine": true
		},
		{
			"files"    : "test/*.spec.js",
			"testName" : "mocha",
			"isMocha": true
		},
		{
        	"files"    : "test/myTest.js",
        	"testName" : "myTest",
        	"isJasmine": false
    	}
	],
	"queueName":"test_queue",  //this is the queue name for your application and is the queue the worker listens to
	"pollInterval":10000    //wait time between polling for incoming messages
}
```

testFiles is an array of tests your application exposes.

For jasmine tests you just supply the location for the spec tests.

For mocha tests you just supply the location for the spec tests.

For non jasmine tests provide the file name of the test file.

When the runner runs your test file it does the following:

```
    require("test/myTest")()
      .then((results)=>{
        ///send the results to outbound sqs queue
        ///delete message from inbound sqs queue
      })
      .catch((results)=>{
         ///send the results to outbound sqs queue
         ///delete message from inbound sqs queue
       });
```
This implies that your test should do the following:
1. export a function
1. When that function is invoked with no arguments return a Promise
1. The promise should ALWAYS resolve even if the test fails. The catch is reserved for an actual code error
1. results should be of the following format (to keep them consistent with format of jasmine tests)

```
  [
    {
      testFile:xxx     // easiest to just populate with __filename
      tests:[
        {
      		testName:xxx,
      		status: passed|failed,
		    reason: human readable message for failure
       }
      ]
    }
  ]
```

