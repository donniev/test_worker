#!/usr/bin/env node
'use strict';
let Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	argv = require("minimist")(process.argv.splice(2)),
	path = require("path"),
	Worker = require("../index.js"),
		aws={};
if(argv.aws){
	aws=require(path.resolve(argv.aws));
}
if(! argv.configfile){
	console.log("no config file provided");
	process.exit(1);
}
//console.log(path.resolve(argv.configfile));
let config=require(path.resolve(argv.configfile));
let worker=new Worker(aws,config);
worker.startListener();
